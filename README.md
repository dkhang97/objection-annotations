# objection-annotations

[![version](https://img.shields.io/npm/v/objection-annotations.svg?style=flat)](https://www.npmjs.com/package/objection-annotations) [![npm](https://img.shields.io/npm/l/objection-annotations.svg)](https://opensource.org/licenses/MIT)

## Installation

```bash
npm i --save objection-annotations
```

## Example

### src/app.ts

```ts
import { Knex } from 'knex';
// import Knex = require('knex');
import { Model } from 'objection';

import { buildTable, Column, Join, Reference, Table } from 'objection-annotations';

@Table('people', { indexes: [['firstName', 'lastName'], 'lastName'] })
export class Person extends Model {
    @Column('increments')
    id: number;

    @Column('string', { schema: { maxLength: 16 }, index: { type: 'fullText' } })
    firstName: string;

    @Column('string', { schema: { maxLength: 16 } })
    lastName: string;

    @Column('string', { schema: { maxLength: 20 }, unique: true })
    phoneNumber: string;

    @Join(() => Todo, 'hasMany', 'personId')
    todo_list: Todo[];
}

@Table('todo_list')
export class Todo extends Model {
    @Column('increments')
    id: number;

    @Column('integer', { index: true })
    @Reference(Person)
    personId: number;

    @Column('date', { index: true })
    expire: Date;

    @Column('text')
    content: string;

    @Join(() => Person, 'belongsTo', 'personId')
    person: Person;
}

(async () => {
    await buildTable(Knex(require('./knexfile.js').staging), Person, Todo);
})();
```

### knexfile.js

```js
module.exports = {
  staging: require('objection-annotations/dist/config').createKnexConfig({
    client: 'sqlite3',
    connection: { filename: 'data.db' },
  }),
};
```