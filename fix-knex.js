try {
    const fsPath = require('path');
    const fs = require('fs');

    const knexLibDir = require('child_process').execSync('npm ls knex --parseable').toString('utf-8').trim();
    const knexLibPkgFile = fsPath.join(knexLibDir, 'package.json');

    if (fs.existsSync(knexLibPkgFile)) {
        const knexPkgContent = fs.readFileSync(knexLibPkgFile, { encoding: 'utf-8' });
        const { version } = JSON.parse(knexPkgContent);

        const { devDependencies } = JSON.parse(fs.readFileSync(fsPath.join(__dirname, 'package.json'), { encoding: 'utf-8' }));


        if (!require('semver').satisfies(version, devDependencies.knex)) {
            const knexDtsFile = fsPath.join(__dirname, 'dist', 'knex.d.ts');
            fs.writeFileSync(knexDtsFile, fs.readFileSync(knexDtsFile, { encoding: 'utf-8' }).split(/\n/).map(line => {
                if (/^\s*import\s+Knex\s*=/.test(line)) {
                    return 'import Knex = KnexLib.Knex;';
                }

                return line;
            }).join('\n'));
        }
    }
} catch (e) { }