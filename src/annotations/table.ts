import { Model, RelationMapping } from 'objection';

import { META_ANN_JOIN, META_ANN_SCHEMA, TableOpts } from '../declarations';
import { appendMeta, getMeta } from '../internals/utils';

export function Table<T extends typeof Model>(tableName: string, opts?: TableOpts) {
    return (target: T) => {
        target.tableName = tableName;
        const joinMeta: RelationMapping<T['prototype']> = getMeta(target, META_ANN_JOIN);

        const { relationMappings: oldMappings } = target;
        target.relationMappings = () => Object.assign({}, oldMappings,
            Object.entries(joinMeta).reduce((prv, [key, metaFn]) => ({
                ...prv, [key]: metaFn(),
            }), {}),
        ) as any;

        if (!target.jsonSchema) {
            target.jsonSchema = {
                type: 'object',
                properties: {},
            };
        }

        appendMeta(target, META_ANN_SCHEMA, opts);
    };
}
