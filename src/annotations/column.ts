import { Model } from 'objection';

import { ColumnOpts, ColumnType, META_ANN_SCHEMA_COL, PickModelField } from '../declarations';
import { appendMeta } from '../internals/utils';

type OAColumnDecorator = (target: Model, propertyKey: string, descriptor?: PropertyDescriptor) => void;

export function Column<T extends typeof Model>(
    type: () => [T | (() => T), PickModelField<T['prototype']>],
    opts?: ColumnOpts,
): OAColumnDecorator;
export function Column(type: ColumnType, opts?: ColumnOpts): OAColumnDecorator;
export function Column(type: string | (() => [typeof Model, string]), opts?: ColumnOpts): OAColumnDecorator {
    return (target, propertyKey) => {
        appendMeta(target, META_ANN_SCHEMA_COL, {
            [propertyKey]: () => ({
                type, opts: typeof opts === 'object' && opts || {},
            }),
        });
    };
}
