import { Model, RelationMapping } from 'objection';

import { META_ANN_JOIN, PickModelField, resolveModelClass } from '../declarations';
import { appendMeta, getMetaDescriptor } from '../internals/utils';

export function Join<T extends typeof Model, TSource extends Model>(targetModelClass: T | (() => T), type: 'hasMany',
    targetField: PickModelField<T['prototype']>, sourceField?: PickModelField<TSource>)
    : (sourceModelClass: TSource, propertyKey: string) => void;
export function Join<T extends typeof Model, TSource extends Model, TThrough extends typeof Model>(
    targetModelClass: T | (() => T), type: 'manyToMany',
    throughEntity: {
        modelClass: TThrough | (() => TThrough),
        from: PickModelField<TThrough['prototype']>,
        to: PickModelField<TThrough['prototype']>,
    },
    sourceField?: string | PickModelField<TSource>, targetField?: PickModelField<T['prototype']>)
    : (sourceModelClass: TSource, propertyKey: string) => void;
export function Join<T extends typeof Model, TSource extends Model>(targetModelClass: T | (() => T), type: 'belongsTo',
    sourceField: string | PickModelField<TSource>, targetField?: PickModelField<T['prototype']>)
    : (sourceModelClass: TSource, propertyKey: string) => void;
export function Join<T extends typeof Model, TThrough extends typeof Model>(modelClass: T | (() => T), type: string,
    throughOrSource: string | { modelClass: TThrough | (() => TThrough), from: string, to: string },
    firstParty: string, secondParty?: string) {
    return (target: Model, propertyKey: string) => {

        const getModelClass = <TM extends typeof Model = T>(m: TM | (() => TM) = modelClass as any) =>
            getMetaDescriptor(resolveModelClass(m)) as TM;

        function extractModels() {
            const { tableName: sourceTable, idColumn: sourceId } = target.constructor as typeof Model;
            const { tableName: targetTable, idColumn: targetId } = getModelClass();
            return { sourceTable, sourceId, targetTable, targetId };
        }

        const appendJoin = (joinFn: () => RelationMapping<any>) =>
            appendMeta(target, META_ANN_JOIN, { [propertyKey]: joinFn });

        switch (type) {
            case 'hasMany':
                appendJoin(() => {
                    const { sourceTable, sourceId, targetTable } = extractModels();

                    return {
                        modelClass: () => getModelClass(),
                        relation: Model.HasManyRelation,
                        join: {
                            from: `${sourceTable}.${firstParty || sourceId}`,
                            to: `${targetTable}.${throughOrSource}`,
                        },
                    };
                });
                break;

            case 'manyToMany':
                if (typeof throughOrSource === 'object') {
                    appendJoin(() => {
                        const { modelClass: thModelClass } = throughOrSource;
                        const { tableName: throughTable } = getModelClass(thModelClass) as TThrough;
                        const { sourceTable, sourceId, targetTable, targetId } = extractModels();

                        return {
                            modelClass: () => getModelClass(),
                            relation: Model.ManyToManyRelation,
                            join: {
                                from: `${sourceTable}.${firstParty || sourceId}`,
                                to: `${targetTable}.${secondParty || targetId}`,
                                through: {
                                    from: `${throughTable}.${throughOrSource.from}`,
                                    to: `${throughTable}.${throughOrSource.to}`,
                                },
                            },
                        };
                    });
                }
                break;

            case 'belongsTo':
                appendJoin(() => {
                    const { sourceTable, sourceId, targetTable } = extractModels();

                    return {
                        modelClass: () => getModelClass(),
                        relation: Model.BelongsToOneRelation,
                        join: {
                            from: `${sourceTable}.${throughOrSource}`,
                            to: `${targetTable}.${firstParty || sourceId}`,
                        },
                    };
                });
                break;
        }
    };
}
