import { Model } from 'objection';

import {
    ForeignKeyInfo,
    META_ANN_HAS_REFS,
    META_ANN_REFS,
    META_ANN_SCHEMA_COL,
    PickModelField,
    ReferenceOpts,
} from '../declarations';
import { appendMeta, getMeta, getMetaDescriptor, pushMeta } from '../internals/utils';
import { resolveModelClass } from '../declarations';

function createModelClassFn<T extends typeof Model>(modelClass: T | (() => T)) {
    let mClass: T;

    return () => {
        if (!mClass) {
            mClass = getMetaDescriptor(resolveModelClass(modelClass)) as any;
        }

        const { tableName, idColumn } = mClass;
        return {
            mClass, tableName,
            idColumn: Array.isArray(idColumn) ? idColumn[0] : idColumn,
        };
    };
}

export function HasReference<T extends typeof Model, TSource extends typeof Model>(
    modelClass: T | (() => T),
    optsOrRefColumn: PickModelField<T['prototype']> | {
        refColumn: PickModelField<T['prototype']>,
        targetColumn?: PickModelField<TSource['prototype']>,
        alterSource?: boolean | 'required'
        foreignKeyName?: string,
        onUpdate?: string,
        onDelete?: string,
    },
): (sourceModelClass: TSource) => void {
    const { refColumn, targetColumn, foreignKeyName, alterSource, onUpdate, onDelete } = typeof optsOrRefColumn === 'object'
        && optsOrRefColumn || (typeof optsOrRefColumn === 'string' && {
            refColumn: optsOrRefColumn,
        } as any) || ({} as any);

    const mFn = createModelClassFn(modelClass);

    return target => {
        pushMeta(target, META_ANN_HAS_REFS, () => {
            const { mClass, tableName } = mFn();

            return {
                foreignKeyName, onUpdate, onDelete,
                alterSource: alterSource === void 0 ? true : alterSource,
                source: {
                    modelClass: mClass,
                    tableName,
                    field: refColumn,
                },
                target: {
                    modelClass: target,
                    tableName: target.tableName,
                    field: targetColumn || (Array.isArray(target.idColumn)
                        ? target.idColumn[0] : target.idColumn),
                },
            } as ForeignKeyInfo;
        });
    };
}

export function Reference<T extends typeof Model>(
    modelClass: T | (() => T),
    optsOrTargetColumn?: PickModelField<T['prototype']> | ReferenceOpts<T>,
) {
    const { targetColumn, foreignKeyName, required, schema, onUpdate, onDelete }: ReferenceOpts<T> = typeof optsOrTargetColumn === 'object'
        && optsOrTargetColumn || (typeof optsOrTargetColumn === 'string' && {
            targetColumn: optsOrTargetColumn,
        } as any) || ({} as any);

    const mFn = createModelClassFn(modelClass);

    return (target: Model, propertyKey: string) => {
        const colSchemas = getMeta(target, META_ANN_SCHEMA_COL);

        if (!colSchemas || !colSchemas[propertyKey]) {
            appendMeta(target, META_ANN_SCHEMA_COL, {
                [propertyKey]: () => ({
                    type() {
                        const { mClass, idColumn } = mFn();
                        return [mClass, targetColumn || idColumn];
                    },
                    opts: { required, schema },
                }),
            });
        }

        appendMeta(target, META_ANN_REFS, {
            [propertyKey]() {
                const { tableName, idColumn, mClass } = mFn();

                return {
                    foreignKeyName, onUpdate, onDelete,
                    source: {
                        modelClass: target['constructor'] as typeof Model,
                        tableName: target['constructor']['tableName'],
                        field: propertyKey,
                    },
                    target: {
                        modelClass: mClass, tableName,
                        field: targetColumn || idColumn,
                    },
                } as ForeignKeyInfo;
            },
        });
    };
}
