import { Knex } from './knex';

export function createKnexConfig(config: Knex.Config): Knex.Config {
    return Object.assign({ useNullAsDefault: true }, config);
}
