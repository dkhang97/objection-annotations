import { Model } from 'objection';
import { Knex } from './knex';

import { META_ANN_REFS, resolveModelClass, TableForeignInfo } from './declarations';
import { assignJsonSchema } from './internals/assign-json-schema';
import { buildTableFromBuilder } from './internals/builder';
import { executeHasRefs } from './internals/builder-has-refs';
import { getMeta } from './internals/utils';

export async function buildTable(knex: Knex, ...modelClasses: typeof Model[]) {
    Model.knex(knex);

    const processedModels: typeof Model[] = [];

    async function createTable(modelClass: typeof Model) {
        const { tableName } = modelClass;

        if (!processedModels.includes(modelClass)
            && !await knex.schema.hasTable(tableName)) {
            const refs: TableForeignInfo = getMeta(modelClass, META_ANN_REFS);

            for (const refKey in refs) {
                const { target: { modelClass: refModelClass } } = refs[refKey]();
                const resolvedRefModelClass = resolveModelClass(refModelClass)

                if (resolvedRefModelClass !== modelClass) {
                    await createTable(resolvedRefModelClass);
                }
            }

            if (await knex.schema.createTable(tableName, builder => {
                buildTableFromBuilder(builder, modelClass, refs);
            }) as any) { processedModels.push(modelClass); }
        }
    }

    for (const m of modelClasses) {
        await createTable(m);
    }

    await executeHasRefs(knex, processedModels);
    modelClasses.forEach(m => assignJsonSchema(m));

    return processedModels;
}
