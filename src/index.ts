import { Model, QueryBuilder } from 'objection';

export type RelatedQueryBuilder<V extends Model> = () => QueryBuilder<V, V>;

export {
    ColumnOpts, TableOpts, ReferenceOpts,
    ColumnUniqueOpts, ColumnIndexOpts,
} from './declarations';
export { buildTable } from './build-table';
export { Table } from './annotations/table';
export { Column } from './annotations/column';
export { HasReference, Reference } from './annotations/reference';
export { Join } from './annotations/join';

export { createKnexConfig } from './config';
