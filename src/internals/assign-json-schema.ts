import { Model } from 'objection';

import { ColumnOpts, META_ANN_SCHEMA_COL } from '../declarations';
import { getJsonSchemaType, getMeta, wrapArray } from './utils';

export function resolveColumnAnnotations<T>({ type, opts }: {
    opts: ColumnOpts, type: string | (() => [T | (() => T), string]),
}, forceType?: boolean) {
    let typeFn = false;
    let refMeta: any;
    let defaultUnsigned: boolean;

    if (typeof type === 'function') {
        const [refModel, refField] = (type as any)();
        const refMetaModel = getMeta(refModel, META_ANN_SCHEMA_COL);
        const refMetaFn = refMetaModel && refMetaModel[refField];
        refMeta = typeof refMetaFn === 'function' && refMetaFn();

        if (!refMeta) {
            return {};
        }

        if (wrapArray(refModel.idColumn).includes(refField)) {
            defaultUnsigned = true;
        }

        type = refMeta['type'] as string;

        typeFn = true;
    }

    if (forceType || typeFn) {
        const orgType = type;

        if (type === 'bigIncrements') {
            type = 'bigInteger';
        } else if (type === 'increments') {
            type = 'integer';
        }

        if (!defaultUnsigned) {
            defaultUnsigned = ['bigIncrements', 'increments'].includes(orgType);
        }

        const { schema, textType, unsigned, required }: ColumnOpts = refMeta && refMeta['opts'] || ({} as any);
        opts = Object.assign({
            textType, unsigned: unsigned === void 0 ? defaultUnsigned : unsigned,
        }, opts || {}, schema && {
            schema: Object.assign(schema, {
                type: getJsonSchemaType(type, !required),
            }, opts && opts.schema),
        });
    }

    return { type, opts };
}

export function assignJsonSchema(target: typeof Model) {
    const colSchemas: Record<string, () => { opts: ColumnOpts, type: string }> =
        getMeta(target, META_ANN_SCHEMA_COL);

    Object.entries(colSchemas)
        .filter(([colName]) => !target.jsonSchema.properties[colName])
        .forEach(([colName, colFn]) => {
            const { type, opts } = resolveColumnAnnotations(colFn(), true);
            target.jsonSchema.properties[colName] = type && opts.schema || {};
        });
}
