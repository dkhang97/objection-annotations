import { Model } from 'objection';

import { META_ANNOTATION } from '../declarations';

export function getJsonSchemaType(colType: string, nullable?: boolean | (() => boolean)) {
    const types = [['tinytext', 'text', 'mediumtext', 'longtext'].includes(colType)
        ? 'string' : colType];

    if (colType === 'bigInteger') {
        types.push('integer');
    }

    const canNull = typeof nullable === 'function' ? nullable() : nullable;

    if (canNull) {
        types.push('null');
    }

    return types.length === 1 ? types[0] : types;
}

export function wrapArray<T>(val: T | T[]): T[] {
    if (Array.isArray(val)) {
        return val;
    }

    return val !== void 0 ? [val] : [];
}

export function getMetaDescriptor<T extends typeof Model>(target: T | T['prototype'] | (() => T)): T {
    const meta = target instanceof Model ? (target['constructor'] as typeof Model) : target;
    return typeof meta === 'object' && (meta && meta['constructor']) || meta as any;
}

export function appendMeta(target: typeof Model | Model, key: string | symbol, metaVal: {}) {
    const meta = getMetaDescriptor(target);
    meta[META_ANNOTATION] = Object.assign({}, meta[META_ANNOTATION]);
    meta[META_ANNOTATION][key] = Object.assign({}, meta[META_ANNOTATION][key], metaVal);
}

export function pushMeta(target: typeof Model | Model, key: string | symbol, ...metaVal: any[]) {
    const meta = getMetaDescriptor(target);
    meta[META_ANNOTATION] = Object.assign({}, meta[META_ANNOTATION]);

    let existedVal: any[] = meta[META_ANNOTATION][key];

    if (!Array.isArray(existedVal)) {
        existedVal = meta[META_ANNOTATION][key] = [];
    }

    existedVal.push(...metaVal);
}

export function getMeta(target: typeof Model | Model, key: string | symbol) {
    const meta = getMetaDescriptor(target);

    const anno = meta[META_ANNOTATION];
    if (anno && typeof anno === 'object') {
        const annoVal = anno[key];

        if (annoVal && typeof annoVal === 'object') {
            return annoVal;
        }
    }
    return {};
}
