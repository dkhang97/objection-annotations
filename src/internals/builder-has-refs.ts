import { Model } from 'objection';
import { Knex } from '../knex';

import { createColumnBuilder } from '../build-column';
import {
    ForeignKeyInfo,
    META_ANN_HAS_REFS,
    META_ANN_REFS,
    META_ANN_SCHEMA_COL,
    META_COL_BUILDER,
    TableForeignInfo,
} from '../declarations';
import { resolveColumnAnnotations } from './assign-json-schema';
import { buildForeignKey } from './builder';
import { appendMeta, getMeta } from './utils';

function alterSourceForForeignKey(builder: Knex.TableBuilder,
    { alterSource, source, target }: ForeignKeyInfo) {
    const meta = getMeta(source.modelClass, META_COL_BUILDER);
    let colProto: Knex.ColumnBuilder = meta && meta[source.field];
    const targetSchemas = getMeta(target.modelClass, META_ANN_SCHEMA_COL);
    const targetSchemaFn = targetSchemas && targetSchemas[target.field];
    const { type: targetType, opts } =
        resolveColumnAnnotations(typeof targetSchemaFn === 'function' && targetSchemaFn() || ({} as any));

    let unsigned = false;
    let type = 'integer';
    const alter = !!colProto;

    if (alter) {
        unsigned = ['increments', 'bigIncrements'].includes(targetType);
    }

    if (targetType && targetType !== 'increments') {
        if (targetType === 'increments') {
            unsigned = true;
        } else if (targetType === 'bigIncrements') {
            unsigned = true;
            type = 'bigInteger';
        } else {
            type = targetType;
        }
    }

    colProto = createColumnBuilder(builder, type, source.field, opts);

    appendMeta(source.modelClass, META_COL_BUILDER, { [source.field]: colProto });

    if (unsigned) {
        colProto.unsigned();
    }

    if (alterSource === 'required') {
        colProto.notNullable();
    }

    if (alter) {
        colProto.alter();
    }

    return colProto;
}

export async function executeHasRefs(knex: Knex, modelClasses: typeof Model[]) {
    const foreignInfos: ForeignKeyInfo[] = [];

    for (const m of modelClasses) {
        const hasRefFns: (() => ForeignKeyInfo)[] = getMeta(m, META_ANN_HAS_REFS);

        if (Array.isArray(hasRefFns)) {
            for (const hasRefFn of hasRefFns) {
                const foreignInfo: ForeignKeyInfo = typeof hasRefFn === 'function' && hasRefFn()
                    || ({} as any);
                const { source, target } = foreignInfo;

                if (source && target && modelClasses.includes(source.modelClass)) {
                    const sourceRefs: TableForeignInfo =
                        getMeta(source.modelClass, META_ANN_REFS);

                    if (!sourceRefs[source.field]) {
                        foreignInfos.push(foreignInfo);
                    }
                }
            }
        }
    }

    const foreignGrp = Object.entries(foreignInfos.reduce((prv, info) => ({
        ...prv, [info.source.tableName]: [...(prv[info.source.tableName] || []), info],
    }), {} as Record<string, ForeignKeyInfo[]>));

    for (const [tableName, infos] of foreignGrp) {
        await knex.schema.alterTable(tableName, builder => {
            infos.filter(info => !!info.alterSource)
                .forEach(info => alterSourceForForeignKey(builder, info));
        });

        await knex.schema.alterTable(tableName, builder => {
            infos.forEach(info => buildForeignKey(builder, info));
        });
    }
}
