import { Model } from 'objection';
import { Knex } from '../knex';

import { createColumnBuilder } from '../build-column';
import {
    ColumnOpts,
    ForeignKeyInfo,
    META_ANN_SCHEMA,
    META_ANN_SCHEMA_COL,
    META_COL_BUILDER,
    TableForeignInfo,
    TableOpts,
} from '../declarations';
import { resolveColumnAnnotations } from './assign-json-schema';
import { appendMeta, getMeta, wrapArray } from './utils';


export function buildForeignKey(builder: Knex.TableBuilder,
    { foreignKeyName, alterSource, source, target, onUpdate, onDelete }: ForeignKeyInfo) {

    let built = builder.foreign(source.field, foreignKeyName)
        .references(target.field).inTable(target.tableName);

    if (typeof onUpdate === 'string') {
        built = built.onUpdate(onUpdate);
    }

    if (typeof onDelete === 'string') {
        built = built.onDelete(onDelete);
    }

    return built;
}

function implTableAnnotation(builder: Knex.CreateTableBuilder,
    { uniques, indexes, keys, engine, charset, collate, inherits, comment }: TableOpts,
    refs: TableForeignInfo): void {

    if (comment ?? false) {
        builder.comment(comment);
    }

    const calcKeys = wrapArray(keys);

    if (calcKeys.length) {
        builder.primary(calcKeys);
    }
    if (Array.isArray(uniques)) {
        uniques.forEach(item => {
            if (item) {
                if (typeof item === 'string') {
                    builder.unique([item]);
                } else if (Array.isArray(item)) {
                    builder.unique(item);
                } else if (typeof item === 'object') {
                    builder.unique(wrapArray(item.fields), item.name);
                }
            }
        });
    }
    if (Array.isArray(indexes)) {
        indexes.forEach(item => {
            if (item) {
                if (typeof item === 'string') {
                    builder.index([item]);
                } else if (Array.isArray(item)) {
                    builder.index(item);
                } else if (typeof item === 'object') {
                    builder.index(wrapArray(item.fields), item.name, item.type);
                }
            }
        });
    }

    if (engine ?? false) {
        builder.engine(engine);
    }

    if (charset ?? false) {
        builder.charset(charset);
    }

    if (collate ?? false) {
        builder.collate(collate);
    }

    if (inherits ?? false) {
        builder.inherits(inherits);
    }

    // foreign key
    Object.values(refs).forEach(refFn => {
        buildForeignKey(builder, refFn());
    });
}


function buildAdditionalColSchema(colProto: Knex.ColumnBuilder, opts: ColumnOpts) {
    if (opts.default !== void 0) {
        colProto.defaultTo(opts.default);
    }

    if (opts.required) {
        colProto.notNullable();
    }

    if (opts.unsigned) {
        colProto.unsigned();
    }

    if (opts.comment) {
        colProto.comment(opts.comment);
    }
}

export function buildTableFromBuilder<T extends typeof Model>(builder: Knex.CreateTableBuilder,
    modelClass: T, refs: TableForeignInfo) {
    const tableOpts: TableOpts = getMeta(modelClass, META_ANN_SCHEMA);
    const colSchemas: Record<string, any> = getMeta(modelClass, META_ANN_SCHEMA_COL);

    const uniques = [...(tableOpts?.uniques ?? [])];
    const indexes = [...(tableOpts?.indexes ?? [])];

    let keys = wrapArray(tableOpts.keys ?? modelClass.idColumn);
    const refKeys = Object.keys(refs);

    for (const colName in colSchemas) {
        const schemaFn = colSchemas[colName];
        const schema = typeof schemaFn === 'function' && schemaFn();
        const { type, opts } = resolveColumnAnnotations(schema, schema && typeof schema['type'] === 'function');

        if (typeof type !== 'string') {
            continue;
        }

        const hasOpt = typeof opts === 'object' && opts;

        const colProto: Knex.ColumnBuilder = createColumnBuilder(builder, type, colName, opts);

        if (type === 'increments' || type === 'bigIncrements') {
            keys = [];
        }

        if (hasOpt) {
            if (keys.includes(colName) || refKeys.includes(colName)) {
                if (opts.unsigned === void 0) {
                    opts.unsigned = true;
                }
            }

            buildAdditionalColSchema(colProto, opts);

            const { unique, index } = opts;
            if (unique) {
                uniques.push(Object.assign({}, typeof unique === 'object' && unique,
                    typeof unique === 'string' && { name: unique }, { fields: colName }));
            }

            if (index) {
                indexes.push(Object.assign({}, typeof index === 'object' && index,
                    typeof index === 'string' && { name: index }, { fields: colName }));
            }
        }

        appendMeta(modelClass, META_COL_BUILDER, { [colName]: colProto });
    }

    implTableAnnotation(builder, { ...tableOpts, uniques, indexes, keys }, refs);
}
