import { ColumnOpts } from "./declarations";
import { Knex } from './knex';

export interface ColumnSchema {
    opts: ColumnOpts;
    type: string;
}

export function createColumnBuilder(tableBuilder: Knex.TableBuilder, type: string, name: string, opts: ColumnOpts): Knex.ColumnBuilder {
    const colParams = (function* () {
        yield name;

        if (opts && typeof opts === 'object') {
            switch (type) {
                case 'text':
                    yield opts.textType;
                    break;

                case 'binary':
                case 'integer':
                case 'tinyint':
                    yield opts.dataLength;
                    break;

                case 'string':
                    yield opts.dataLength ?? opts.schema?.maxLength;
                    break;

                case 'enu':
                case 'enum':
                    yield opts?.enumValues;
                    yield opts?.enumOptions;
                    break;

                case 'float':
                case 'double':
                case 'decimal':
                    yield opts?.floatPrecision;
                    yield opts?.floatScale;
                    break;

                case 'dateTime':
                case 'timestamp':
                    yield opts?.timeOptions;
                    break;
            }
        }
    })();

    return tableBuilder[type].apply(tableBuilder, Array.from(colParams));
}