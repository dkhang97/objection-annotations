import { Knex } from './knex';
import { JSONSchema, Model } from 'objection';


export interface ForeignKeyInfo {
    foreignKeyName: string;
    onUpdate?: string
    onDelete?: string;
    alterSource?: boolean | 'required';
    source: {
        modelClass: typeof Model,
        tableName: string,
        field: string,
    };
    target: {
        modelClass: typeof Model,
        tableName: string,
        field: string,
    };
}

export type TableForeignInfo = Record<string, () => ForeignKeyInfo>;

export type PickModelField<T extends Model> = Exclude<{ [P in keyof T]: T[P] extends (string | number | boolean)
    ? P : never }[keyof T], keyof Model>;

export const META_ANNOTATION = Symbol('object_annotation_meta');
export const META_ANN_SCHEMA = Symbol('schemas');
export const META_ANN_SCHEMA_COL = Symbol('col_schemas');
export const META_ANN_JOIN = Symbol('joins');
export const META_ANN_REFS = Symbol('refs');
export const META_ANN_HAS_REFS = Symbol('has_refs');
export const META_COL_BUILDER = Symbol('knex_col_builder');

export type ColumnType = {
    [K in keyof Knex.TableBuilder]: Knex.TableBuilder[K] extends ((columnName: string) => Knex.ColumnBuilder) ? K : never
}[keyof Omit<Knex.TableBuilder, 'dropTimestamps'>];

export interface ColumnUniqueOpts {
    fields: string | string[];
    name?: string;
}

export interface ColumnIndexOpts extends ColumnUniqueOpts {
    type?: string;
}

export interface TableOpts {
    uniques?: (string | string[] | ColumnUniqueOpts)[];
    indexes?: (string | string[] | ColumnIndexOpts)[];
    keys?: string | string[];
    engine?: string;
    charset?: string;
    collate?: string;
    inherits?: string;
    comment?: string;
}

export interface ColumnOpts {
    // columnName?: string; // TODO: need to check if knex can re-map column
    comment?: string;

    required?: boolean;
    index?: boolean | string | Omit<ColumnIndexOpts, 'fields'>;
    unique?: boolean | string | Omit<ColumnUniqueOpts, 'fields'>;
    default?: Knex.Value | null;

    /**
     * specify length for `string`, `integer`, `tinyint` and `binary`
     */
    dataLength?: number;

    /**
     * for `integer`
     */
    unsigned?: boolean;

    /**
     * for `float`, `double` and `decimal`
     */
    floatPrecision?: number;
    /**
     * for `float`, `double` and `decimal`
     */
    floatScale?: number;

    /**
     * for `dateTime` and `timestamp`
     */
    timeOptions?: { useTz?: boolean, precision?: number };

    /**
     * for `text`
     */
    textType?: 'tinytext' | 'text' | 'mediumtext' | 'longtext';

    /**
     * for `enum`, `enu`
     */
    enumValues?: Knex.Value[];
    /**
     * for `enum`, `enu`
     */
    enumOptions?: Knex.EnumOptions;

    schema?: JSONSchema;
}

export interface ReferenceOpts<T extends typeof Model> {
    targetColumn?: PickModelField<T['prototype']>;
    foreignKeyName?: string;
    schema?: JSONSchema;
    required?: boolean;
    onUpdate?: string
    onDelete?: string;
}

export function resolveModelClass<T extends typeof Model>(m: T | (() => T)): T {
    if (typeof m === 'function') {
        if (m['__proto__'] === Model) {
            return m as any;
        }

        const resolved = (m as any)();
        return typeof resolved === 'function' ? resolved : m;
    }
}